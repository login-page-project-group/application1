package com.example.application1.payload;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeResponse {

    private int id;

    private String name;

    private String email;
}
